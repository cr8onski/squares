let high = window.innerHeight;
let wide = window.innerWidth;
let dimension;
let minBorder = 35;
let numBoxes = 15;
let highScore = 0;
let moveCounter = 0;
let score = 0;
const LEVEL0 = 16;
const LEVEL1 = 9;
const LEVEL2 = 4;
const LEVEL3 = 1;

// Really not expecting to have more than one body
let board = document.getElementById('gameboard');
// console.log(board);
board.innerText = '';

if (wide < high) {
    dimension = Math.floor((wide - (2 * minBorder)) / numBoxes);
    let tb = wide - (2 * minBorder);
    let lr = high - wide - (2 * minBorder);
    board.style.margin = `${tb}px ${lr}px`;
} else {
    dimension = Math.floor((high - (2 * minBorder)) / numBoxes);
}

for (let i = 0; i < numBoxes; i++) {
    for (let k = 0; k < numBoxes; k++) {
        let box = document.createElement('div');
        box.classList.add('box');
        box.dataset.row = i;
        box.dataset.col = k;
        box.style.width = dimension;
        box.style.height = dimension;
        board.append(box);
    }
}

// Set up controls
let up = document.getElementById('upBtn');
let upRight = document.getElementById('upRightBtn');
let right = document.getElementById('rightBtn');
let downRight = document.getElementById('downRightBtn');
let down = document.getElementById('downBtn');
let downLeft = document.getElementById('downLeftBtn');
let left = document.getElementById('leftBtn');
let upLeft = document.getElementById('upLeftBtn');
let center = document.getElementById('centerBtn');
let reset = document.getElementById('resetBtn');
let powerBlast = document.getElementById('powerBlastBtn');
let blindWarp = document.getElementById('blindWarpBtn');
// note: two steps seemed like a necessary option at hte start, but with changing how the pawn chase the player
//       now it seems semi pointless and not really helpful, I'll give it some time and probably just rip it out
// let twoSteps = document.getElementById('twoStepsBtn');

up.addEventListener('click', e => movePlayer(e, 0, -1));
upRight.addEventListener('click', e => movePlayer(e, 1, -1));
right.addEventListener('click', e => movePlayer(e, 1, 0));
downRight.addEventListener('click', e => movePlayer(e, 1, 1));
down.addEventListener('click', e => movePlayer(e, 0, 1));
downLeft.addEventListener('click', e => movePlayer(e, -1, 1));
left.addEventListener('click', e => movePlayer(e, -1, 0));
upLeft.addEventListener('click', e => movePlayer(e, -1, -1));
center.addEventListener('click', e => movePlayer(e, 0, 0));
reset.addEventListener('click', resetGameboard);
powerBlast.addEventListener('click', powerBlastMove);
blindWarp.addEventListener('click', blindWarpMove);
// twoSteps.addEventListener('click', e => movePlayer(e, 2, 2));

addPlayer();
addPawns(80);
updateScoreboard();


// console.log(wide, 'x', high, dimension);

function addPlayer () {
    let row = Math.floor(Math.random() * numBoxes);
    let col = Math.floor(Math.random() * numBoxes);
    let boxes = document.getElementsByClassName('box');
    for (let box of boxes) {
        if (box.dataset.row == row && box.dataset.col == col) {
            box.classList.add('player');
        }
    }
    if (blindWarp.hasAttribute('disabled')) {
        blindWarp.removeAttribute('disabled');
    }
    if (powerBlast.hasAttribute('disabled')) {
        powerBlast.removeAttribute('disabled');
    }
}

function addPawns (numPawns = 1) {
    for (let i = 0; i < numPawns; i++) {
        let row = Math.floor(Math.random() * numBoxes);
        let col = Math.floor(Math.random() * numBoxes);
        let boxes = document.getElementsByClassName('box');
        for (let box of boxes) {
            if (box.dataset.row == row && box.dataset.col == col) {
                if (box.classList.contains('player') || box.classList.contains('pawn')) {
                    i--;
                } else {
                    box.classList.add('pawn');
                }
            }
        }
    }
    score = 0;
}

function movePlayer (e, horz, vert) {
    e.stopPropagation();
    let player = document.getElementsByClassName('player')[0];
    let row = +player.dataset.row + vert;
    let col = +player.dataset.col + horz;
    if (row >= 0 && row < numBoxes && col >= 0 && col < numBoxes) {
        row = row.toString();
        col = col.toString();
        player.classList.remove('player');
        let boxes = document.getElementsByClassName('box');
        for (let box of boxes) {
            if (box.dataset.row == row && box.dataset.col == col) {
                if (box.classList.length > 1) {
                    box.classList = 'box collision';
                    return gameOver();
                } else {
                    box.classList.add('player');
                    moveCounter++;
                }
            }
        }
    }
    movePawns(+player.dataset.row, +player.dataset.col);
}

function powerBlastMove (e) {
    e.stopPropagation();
    e.target.setAttribute('disabled', 'disabled');
    let boxes = document.getElementsByClassName('box');
    let player = document.getElementsByClassName('player')[0];
    for (let box of boxes) {
        // console.log(box, player);
        let rowProximity = Math.abs(box.dataset.row - player.dataset.row);
        let colProximity = Math.abs(box.dataset.col - player.dataset.col);
        if (rowProximity == 0 && colProximity == 0) {
            // do nothing
        } else if (rowProximity <= 1 && colProximity <= 1) {
            if (box.classList.contains('pawn')) {
                score ++;
            }
            box.classList = 'box collision';
            setTimeout(() => {box.classList = 'box'}, 500);
        }
    }
    setTimeout(() => {++moveCounter;movePawns(+player.dataset.row, +player.dataset.col);}, 600);
}

function blindWarpMove (e) {
    e.stopPropagation();
    // if (e.target.hasAttribute('disabled')) return; //actually this is now unnecessary
    e.target.setAttribute('disabled', 'disabled');
    let oldPlayer = document.getElementsByClassName('player')[0];
    /**
     * I want to choose a random space
     * make sure it is not the current space
     * make sure it is not a pawn
     * move the player there and remove the player from where it was
     * -so what if I grab all the boxes filter anything with more than box use the length of that array to be the random upper limit, place the player there and remove the player from where it was
     * now we proceed with moving the pawns
     */
    let freeBoxes = [...document.getElementsByClassName('box')].filter(x => !x.classList.contains('player')).filter(x => !x.classList.contains('pawn'));
    let player = freeBoxes[Math.floor(Math.random() * freeBoxes.length)];
    // console.log(freeBoxes, player);
    oldPlayer.classList.remove('player');
    player.classList.add('player');
    moveCounter++;
    movePawns(+oldPlayer.dataset.row, +oldPlayer.dataset.col);
}

function movePawns (playerRow, playerCol) {
    // let player = document.getElementsByClassName('player')[0];
    // let playerRow = player.dataset.row;
    // let playerCol = player.dataset.col;
    let boxes = document.getElementsByClassName('box');
    let pawns = document.getElementsByClassName('pawn');
    let oldPositions = [];
    let newPositions = [];
    for (let p in pawns) {
        if (pawns.hasOwnProperty(p)) {
            let pawn = pawns[p];
            oldPositions.push({row: pawn.dataset.row, col: pawn.dataset.col});
            let pawnRow = pawn.dataset.row;
            let pawnCol = pawn.dataset.col;
            if (pawnRow != playerRow) {
                changed = true;
                if (pawnRow < playerRow) {
                    pawnRow = +pawnRow + 1;
                } else {
                    pawnRow = +pawnRow - 1;
                }
                if (pawnRow < 0) {
                    pawnRow = '0';
                } else if (pawnRow >= numBoxes) {
                    pawnRow = numBoxes;
                }
                pawnRow = pawnRow.toString();
            }
            if (pawnCol != playerCol) {
                changed = true;
                if (pawnCol < playerCol) {
                    pawnCol = +pawnCol + 1;
                } else {
                    pawnCol = +pawnCol - 1;
                }
                if (pawnCol < 0) {
                    pawnCol = '0';
                } else if (pawnCol >= numBoxes) {
                    pawnCol = numBoxes;
                }
                pawnCol = pawnCol.toString();
            }
            newPositions.push({row: pawnRow, col: pawnCol});
        }
    }

    for (let i = pawns.length - 1; i >= 0; i--) {
        pawns[i].classList.remove('pawn');
    }

    for (let position of newPositions) {
        for (let box of boxes) {
            if (box.dataset.row == position.row && box.dataset.col == position.col) {
                box.classList += ' pawn';
            }
        }
    }
    collisionCheck(playerRow, playerCol);
}

function collisionCheck (playerRow, playerCol) {
    let boxes = document.getElementsByClassName('box');
    let gameover = false;
    let startScore = score;
    for (let box of boxes) {
        let classes = box.classList.value.split(' ')
        if (classes.length >= 3) {
            if (box.classList.contains('player')) {
                gameover = true;
                score = startScore;
            } 
            if(!gameover) {
                // I need the number of pawns in that box
                // I can cheat, I know that the classlist contains box, but not player, nor collision which are the only classes other than pawn which are applied to the boxes
                // so subtract 1 from the length of the classes array and I've got the number of boxes
                let numPawns = classes.length - 1;
                // and I need the proximity of that box to the player
                let rowProximity = Math.abs(box.dataset.row - playerRow);
                let colProximity = Math.abs(box.dataset.col - playerCol);
                if (rowProximity == 0 && colProximity == 0) {
                    score += numPawns * LEVEL0;
                } else if (rowProximity <= 1 && colProximity <= 1) {
                    score += numPawns * LEVEL1;
                } else if (rowProximity <= 2 && colProximity <= 2) {
                    score += numPawns * LEVEL2;
                } else {
                    score += numPawns * LEVEL3;
                }
            }

            box.classList = 'box collision';
            if (!gameover) {
                setTimeout(() => {box.classList = 'box';}, 500);
            }
        }
    }
    if (gameover) {
        gameOver();
    }
    updateScoreboard(score, moveCounter, highScore);
}

function gameOver () {
    console.log('Game Over');
    highScore = score > highScore ? score : highScore;
    // score = 0;
    updateScoreboard(score, moveCounter, highScore);
}

function resetGameboard (e) {
    e.stopPropagation();
    clearBoard();
    moveCounter = 0;
    highScore = score > highScore ? score : highScore;
    score = 0;
    updateScoreboard(score, moveCounter, highScore);
    addPlayer();
    addPawns(100);
    postEnemies();
}

function clearBoard () {
    let boxes = document.getElementsByClassName('box');
    for (let box of boxes) {
        box.classList = 'box';
    }
}

// Scoreboard
function updateScoreboard (score = 0, moves = 0, highScore = 11) {
    postScore(score);
    postMoves(moves);
    postEnemies();
    postHighScore(highScore);
}
function postScore (score = 0) {
    document.getElementById('numScore').innerText = score;
}
function postMoves (moves = 0) {
    document.getElementById('numMoves').innerText = moves;
}
function postEnemies (enemies = 0) {
    enemies = document.getElementsByClassName('pawn').length;
    document.getElementById('numEnemies').innerText = enemies;
}
function postHighScore (highScore = 0) {
    document.getElementById('numHighScore').innerText = highScore;
}
